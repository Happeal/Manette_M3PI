package com.example.i_p34lounet.manettem3pi;

import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

/**
 * Created by nassim on 27/12/2016.
 * classe qui heberge tous les elements de l'interface graphique du controleur
 *
 * @see android.app.Fragment
 */

public class ControllerFragment extends Fragment {
    /*le profil par defaut, c'est a dire le profil SP Serial Profile*/
    static final UUID BASE_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    /*code de requete Bluetooth*/
    static final int PERMISSION_REQUEST_CODE = 2016;
    /*code de requete d'activation de la radio Bluetooth*/
    static final int ENABLE_REQUEST_CODE = 2017;
    /*le nom a utiliser pour rechercher la Raspberry Pi par Bluetooth*/
    static final String RASPBERRYPI_NAME = "raspberrypi";
    /*activer ou non le mode de debogage*/
    private static final boolean DEBUG = true;
    /*un titre a utiliser pour se reperer dans les textes de debogage*/
    private static String TAG = "ControllerFragment";
    /*reference vers le bouton de commande "marche avant"*/
    private Button mForwardButton = null;
    /*booleen qui permet de savoir si oui ou non l'utilisateur appuie sur le bouton de commande
    "marche avant"*/
    private volatile boolean mForwardPressed = false;
    /*reference vers le bouton de commande "marche arriere"*/
    private Button mBackwardButton = null;
    /*boolean qui permet de savoir si oui ou non l'utilisateur appuie sur le bouton de commande
    "marche arriere"*/
    private volatile boolean mBackwardPressed = false;
    /*reference vers le bouton de commande "rotation a droite"*/
    private Button mRightButton = null;
    /*booleen qui permet de savoir si oui ou non l'utilisateur appuie sur le bouton de commande
    "rotation a droite"*/
    private volatile boolean mRightPressed = false;
    /*reference vers le bouton de commande "rotation a gauche"*/
    private Button mLeftButton = null;
    /*booleen qui permet de savoir si oui ou non l'utilisateur appuie sur le bouton de commande
    "rotation vers la gauche"*/
    private volatile boolean mLeftPressed = false;
    /*reference vers une dispositif Bluetooth (ici sera la Raspberry Pi)*/
    private BluetoothDevice mDevice = null;
    /*entier contenant le niveau de la batterie retournee par le robot Pololu(TM) m3pi*/
    private int mBatteryLevel = 0;
    /*une reference vers un tunnel de communication RFCOMM Bluetooth*/
    private BluetoothSocket mSocket = null;
    /*un boolean qui permet de savoir s'il faut arreter les taches (threads) ou non*/
    private boolean mStopTrigger = false;
    /*une reference vers un champs de texte qui affichera le niveau de la batterie retourne par
    le robot Pololu(TM) m3pi*/
    private TextView mBatteryLevelText = null;
    /*un booleen qui permet de savoir si oui ou non on est connecte a la Raspberry Pi*/
    private boolean mIsConnected = false;
    /*entier qui indique l'intervalle de rafraichissement du niveau de la batterie*/
    private int mBatteryCheckTime = 10000;
    /*reference vers une tache de lancement des taches d'envoi de commandes et de lecture du
    niveau de la batterie*/
    private Thread mFragmentConnectionEstablisher = null;

    /**
     * methode statique qui genere une nouvelle instance du ControllerFragment
     *
     * @return une nouvelle instance du ControllerFragment
     */
    public static ControllerFragment newInstance() {
        return new ControllerFragment();
    }

    /**
     * methode statique qui retourne l'identifiant du profil par defaut
     *
     * @return l'identifiant unique du profile par defaut
     */
    public synchronized static UUID getBaseUuid() {
        return BASE_UUID;
    }

    /**
     * methode qui retourne le code de permission Bluetooth
     *
     * @return le code de permission Bluetooth
     */
    public synchronized static int getPermissionRequestCode() {
        return PERMISSION_REQUEST_CODE;
    }

    /**
     * methode statique qui retourne le code de demande d'activation de la radio Bluetooth
     */
    public synchronized static int getEnableRequestCode() {
        return ENABLE_REQUEST_CODE;
    }

    /**
     * methode statique qui retourne le nom du dispositif Bluetooth (ici la Raspberry Pi)
     *
     * @return le nom du dispositif Bluetooth (ici la Raspberry Pi)
     */
    public synchronized static String getRaspberrypiName() {
        return RASPBERRYPI_NAME;
    }

    /**
     * methode qui retourne l'etat d'appui des boutons de commandes
     *
     * @param id l'identifiant du bouton dont on cherche l'etat d'appui
     * @return true si l'utilisateur a le doigt sur le bouton identifie par id
     */
    public synchronized boolean ismTrigger(int id) {
        boolean result = false;
        switch (id) {
            /*bouton "marche avant"*/
            case 1:
                result = mForwardPressed;
                break;
            /*bouton "marche arriere"*/
            case 2:
                result = mBackwardPressed;
                break;
            /*bouton "rotatio a gauche"*/
            case 3:
                result = mLeftPressed;
                break;
            /*bouton "rotation a droite"*/
            case 4:
                result = mRightPressed;
                break;
        }
        return result;
    }

    /**
     * methode qui retourne la valeur de l'intervalle de rafrachissement du niveau de la batterie
     *
     * @return l'intervalle de rafraichissement du niveau de la batterie
     */
    public synchronized int getmBatteryCheckTime() {
        return mBatteryCheckTime;
    }

    /**
     * methode qui modifie l'intervalle de rafraichissement du niveau de la batterie
     */
    public synchronized void setmBatteryCheckTime(int mBatteryCheckTime) {
        this.mBatteryCheckTime = mBatteryCheckTime;
    }

    /**
     * methode qui retourne l'etat de la connexion de la radio Bluetooth
     *
     * @return l'etat de la connexion de la radio Bluetooth
     */
    public synchronized boolean ismIsConnected() {
        return mIsConnected;
    }

    /**
     * methode qui modifie l'etat de la connexion de la radio Bluetooth
     *
     * @param mIsConnected l'etat de connexion desire
     */
    public synchronized void setmIsConnected(boolean mIsConnected) {
        this.mIsConnected = mIsConnected;
        if (DEBUG)
            Log.d(TAG, "Application connected to Raspberry");
    }

    /**
     * methode qui retourne l'etat du bouton "marche avant" en appui
     *
     * @return true si le bouton est appuye. false sinon.
     */
    public synchronized boolean ismForwardPressed() {
        return mForwardPressed;
    }

    /**
     * methode qui modifie l'etat en appui du bouton "marche avant"
     *
     * @param mForwardPressed l'etat en appui desire
     */
    public synchronized void setmForwardPressed(boolean mForwardPressed) {

        this.mForwardPressed = mForwardPressed;
        if (DEBUG) {
            if (mForwardPressed)
                Log.d(TAG, "Forward button pressed");
            else
                Log.d(TAG, "Forward button released");
        }
    }

    /**
     * methode qui retourne l'etat du bouton "marche arriere" en appui
     *
     * @return true si le bouton est appuye. false sinon.
     */
    public synchronized boolean ismBackwardPressed() {
        return mBackwardPressed;
    }

    /**
     * methode appelee lors de la creation
     *
     * @param savedInstanceState le dernier etat de l'instance du ControllerFragment sauvegardee
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    /**
     * methode qui modifie l'etat en appui du bouton "marche arriere"
     *
     * @param mBackwardPressed l'etat en appui desire
     */
    public synchronized void setmBackwardPressed(boolean mBackwardPressed) {
        this.mBackwardPressed = mBackwardPressed;
        if (DEBUG) {
            if (mBackwardPressed)
                Log.d(TAG, "Backward button pressed");
            else
                Log.d(TAG, "Backward button released");
        }
    }


    /**
     * methode qui retourne la reference vers le champs de texte du niveau de la batterie
     *
     * @return la reference vers le champs de texte du niveau de la batterie
     */
    public TextView getmBatteryLevelText() {
        return mBatteryLevelText;
    }

    /**
     * methode qui modifie la reference vers le champs de texte reserve a l'affichage du niveau
     * de la batterie
     *
     * @param mBatteryLevelText la reference vers le chempas de texte reserve a l'affichage du
     *                          niveau de la batterie
     */
    public void setmBatteryLevelText(TextView mBatteryLevelText) {
        this.mBatteryLevelText = mBatteryLevelText;
    }

    /**
     * methode qui etablit une connexion Bluetooth avec la Raspberry Pi
     */
    void establishConnectionWithPi() {
        /*creation d'une nouvelle tache qui executera le code d'une instance du
        ConnectionEstablisher*/
        mFragmentConnectionEstablisher = new Thread(new ConnectionEstablisher(this));
        /*demarrer la tache de connexion*/
        mFragmentConnectionEstablisher.start();
        /*attacher au bouton "marche avant" un ecouteur d'evenement MyOnTouchListener"*/
        mForwardButton.setOnTouchListener(new MyOnTouchListener(1));
        /*attacher au bouton "marche arriere" un ecouteur d'evenement MyOnToucheListener"*/
        mBackwardButton.setOnTouchListener(new MyOnTouchListener(2));
        /*attacher au bouton "rotation a gauche" un ecouteur d'evenement MyOnToucheListener"*/
        mLeftButton.setOnTouchListener(new MyOnTouchListener(3));
        /*attacher au bouton "rotation a droite" un ecouteur d'evenement MyOnToucheListener"*/
        mRightButton.setOnTouchListener(new MyOnTouchListener(4));

    }

    /**
     * methode qui retourne le dispositif Bluetooth cible (ici la Raspberry Pi)
     *
     * @return une reference vers le dispositif Bluetooth
     */
    public synchronized BluetoothDevice getmDevice() {
        return mDevice;
    }

    /**
     * methode qui retourne une reference vers le bouton "marche avant"
     *
     * @return une reference vers le bouton "marche avant"
     */
    public Button getmForwardButton() {
        return mForwardButton;
    }

    /**
     * methode qui modifie la reference vers le bouton "marche avant"
     *
     * @param mForwardButton reference verse le bouton "marche avant"
     */
    public void setmForwardButton(Button mForwardButton) {
        this.mForwardButton = mForwardButton;
    }


    /**
     * methode qui retourne l'etat en appui du bouton "rotation a droite"
     *
     * @return l'etat en appui du bouton "rotation a droite"
     */
    public synchronized boolean ismRightPressed() {
        return mRightPressed;
    }

    /**
     * methode qui modifie l'etat en appui du bouton "marche avant"
     *
     * @param mRightPressed l'etat en appui desire
     */
    public synchronized void setmRightPressed(boolean mRightPressed) {
        this.mRightPressed = mRightPressed;
        if (DEBUG) {
            if (mRightPressed)
                Log.d(TAG, "Right button pressed");
            else
                Log.d(TAG, "Right button released");
        }
    }

    /**
     * methode qui retourne l'etat en appui du bouton "rotation a gauche"
     *
     * @return l'etat en appui du bouton "rotation a gauche"
     */
    public synchronized boolean ismLeftPressed() {
        return mLeftPressed;
    }

    /**
     * methode qui modifie l'etat en appui du bouton "rotation a gauche"
     *
     * @param mLeftPressed l'etat en appui desire
     */
    public synchronized void setmLeftPressed(boolean mLeftPressed) {
        this.mLeftPressed = mLeftPressed;
        if (DEBUG) {
            if (mLeftPressed)
                Log.d(TAG, "Left button pressed");
            else
                Log.d(TAG, "Left button released");
        }
    }

    /**
     * methode qui retourne l'etat de l'ordre d'arrêt des taches
     *
     * @return l'etat de l'ordre d'arrêt des taches
     */
    public synchronized boolean ismStopTrigger() {
        return mStopTrigger;
    }

    /**
     * methode qui modifie l'etat de l'ordre d'arrêt des taches
     *
     * @param mStopTrigger l'etat de l'ordre d'arrêt des taches
     */
    public synchronized void setmIsStopTrigger(boolean mStopTrigger) {
        this.mStopTrigger = mStopTrigger;
        try {
            mSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * methode qui retourne le niveau de la batterie du robot Pololu(TM) m3pi
     *
     * @return le niveau de la batterie du robot Pololu(TM) m3pi
     */
    public synchronized int getmBatteryLevel() {
        return mBatteryLevel;
    }

    /**
     * methode qui modifie le niveau de la batterie du robot Pololu(TM) m3pi
     *
     * @param mBatteryLevel lle niveau de la batterie du robot Pololu(TM) m3pis
     */
    public synchronized void setmBatteryLevel(int mBatteryLevel) {
        this.mBatteryLevel = mBatteryLevel;
        /*afficher le texte a l'utilisateur*/
        this.mBatteryLevelText.setText(mBatteryLevel + "%");
        if (DEBUG)
            Log.d(TAG, "Battery level updated");
    }

    /**
     * methode appelee lors de la creation d'une interface graphique pour l'instance du
     * ControllerFragment
     *
     * @param inflater           le "gonfleur" d'interface graphiques
     * @param container          le groupe de Vues qui hébergera la vue en creation
     * @param savedInstanceState le dernier etat de l'instance sauvegardee
     * @return une vue pour l'instance appelante
     * @see Fragment
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.controller_layout, container, false);
    }

    /**
     * methode qui verifie si oui ou non les demandes de permissions sont declarees par
     * l'application
     *
     * @param permission le nom de la permission
     * @return le code de l'etat de la declaration de la permission
     */
    private int checkForPermissions(String permission) {
        return ContextCompat.checkSelfPermission(getActivity(), permission);

    }

    /**
     * methode appelee lors de la disparition de l'ecran actuel de l'affichage utilisateur
     */
    @Override
    public void onStop() {
        super.onStop();
        /*signifie que les taches doivent s'arreter*/
        setmIsStopTrigger(true);
    }

    /**
     * methode appele apres que l'interface graphique ait ete cree
     *
     * @param view               la vue en question
     * @param savedInstanceState le dernier etat de l'instance sauvegardee
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*recuperer l'ensemble des parametres partages identifies CONFIGURATION_IDENTIFIER*/

        /*recuperer la valeur sauvegardee de l'intervalle d'echantillonnage du niveau de la
        batterie*/

        /*recuperer la reference vers le bouton "marche avant"*/
        mForwardButton = (Button) getActivity().findViewById(R.id.forward_button);
         /*recuperer la reference vers le bouton "marche arriere"*/
        mBackwardButton = (Button) getActivity().findViewById(R.id.backward_button);
         /*recuperer la reference vers le bouton "rotation a droite"*/
        mRightButton = (Button) getActivity().findViewById(R.id.turn_right_button);
         /*recuperer la reference vers le bouton "rotation a gauche"*/
        mLeftButton = (Button) getActivity().findViewById(R.id.turn_left_button);
         /*recuperer la reference vers le champs de texte "niveau de la batterie"*/
        mBatteryLevelText = (TextView) getActivity().findViewById(R.id.battery_level_textView);
        /*recuperer la reference vers l'adaptateur Bluetooth du telephone*/
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        /*recuperer la liste de tous les dispositifs Bluetooth paires a ce telephone*/
        Set<BluetoothDevice> bondedDevices =
                BluetoothAdapter.getDefaultAdapter().getBondedDevices();
        /*recuperer la reference vers la radio Bluetooth de la Rapsberry Pi*/
        for (BluetoothDevice device : bondedDevices) {
            if (device.getName().equals(ControllerFragment.RASPBERRYPI_NAME)) {
                mDevice = device;
                break;
            }
        }
        /*si la radio Bluetooth de la Raspberry est pairee*/
        if (mDevice != null)
            /*etablir la connexion Bluetooth avec la Raspberry Pi*/
            establishConnectionWithPi();
    }


    /**
     * methode qui retourne la reference vers le tunnel de communication Bluetooth RFCOMM
     *
     * @return la reference vers le tunnel de communication Bluetooth RFCOMM
     */
    public synchronized BluetoothSocket getmSocket() {
        return mSocket;
    }

    /**
     * methode qui modifie la reference vers le tunnel de communication Bluetooth RFCOMM
     *
     * @param mSocket le tunnel de communication Bluetooth RFCOMM
     */
    public synchronized void setmSocket(BluetoothSocket mSocket) {
        this.mSocket = mSocket;
    }

    /**
     * classe privee qui implement l'interface {@link View.OnTouchListener}
     *
     * @see android.view.View.OnTouchListener
     */
    private class MyOnTouchListener implements View.OnTouchListener {
        /*le numero associe a un des boutons de l'interface graphique de controle*/
        private int mmButtonNumber = 0;

        /**
         * constructeur
         *
         * @param buttonNumber le numero du bouton dont l'evenement du toucher est a surveiller
         */
        public MyOnTouchListener(int buttonNumber) {
            this.mmButtonNumber = buttonNumber;
        }

        /**
         * methode appele lorsque l'utilisateur touche le bouton surveille
         *
         * @param view        la vue (ici le bouton)
         * @param motionEvent informations sur l'evenement tactile
         * @return false
         */
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            /*si l'utilisateur a le doigt sur le bouton*/
            if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                /*associer l'action a appliquer avec le bon bouton*/
                switch (mmButtonNumber) {
                    /*si c'est le bouton "marche avant"*/
                    case 1://forward
                        /*notifier la tache associee de l'etat d'appui de ce dernier*/
                        ControllerFragment.this.setmForwardPressed(true);
                        break;
                    /*si c'est le bouton "marche arriere"*/
                    case 2://backward
                        /*notifier la tache associee de l'etat d'appui de ce dernier*/
                        ControllerFragment.this.setmBackwardPressed(true);
                        break;
                    /*si c'est le bouton "rotation a gauche"*/
                    case 3://left
                        /*notifier la tache associee de l'etat d'appui de ce dernier*/
                        ControllerFragment.this.setmLeftPressed(true);
                        break;
                    /*si c'est le bouton "rotation a droite"*/
                    case 4://right
                        /*notifier la tache associee de l'etat d'appui de ce dernier*/
                        ControllerFragment.this.setmRightPressed(true);
                        break;
                }
                /*si l'utilisateur leve le doigt du bouton*/
            } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                /*associer l'action a appliquer avec le bont bouton*/
                switch (mmButtonNumber) {
                    /*si c'est le bouton "marche avant"*/
                    case 1://forward
                        /*notifier la tache associee de l'etat d'appui de ce dernier*/
                        ControllerFragment.this.setmForwardPressed(false);
                        break;
                    /*si c'est le bouton "marche arriere"*/
                    case 2://backward
                        /*notifier la tache associee de l'etat d'appui de ce dernier*/
                        ControllerFragment.this.setmBackwardPressed(false);
                        break;
                    /*si c'est le bouton "rotation a gauche"*/
                    case 3://left
                        /*notifier la tache associee de l'etat d'appui de ce dernier*/
                        ControllerFragment.this.setmLeftPressed(false);
                        break;
                    /*si c'est le bouton "rotation a droite"*/
                    case 4://right
                        /*notifier la tache associee de l'etat d'appui de ce dernier*/
                        ControllerFragment.this.setmRightPressed(false);
                        break;
                }
            }
            return false;
        }
    }
}
