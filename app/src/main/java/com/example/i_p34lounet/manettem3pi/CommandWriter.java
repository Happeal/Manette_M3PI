package com.example.i_p34lounet.manettem3pi;

import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by nassim on 27/12/2016.
 * classe implementant {@link Runnable}
 *
 * @see Runnable
 */

public class CommandWriter implements Runnable {
    /*titre a utiliser pour reconnaitre les textes de debogages associes a cette classe*/
    private static final String TAG = "CommandWriter";
    /*active ou desactive le mode debogage*/
    private static final boolean DEBUG = true;
    /*le temps de sommeil en millisecondes de la tache executant le code ce Runnable*/
    private static final long SLEEP_TIME = 500;
    /*reference vers le contexte de la tache executant le code de ce Runnable*/
    private ControllerFragment mContext;
    /*reference vers un tunnel de communication Bluetooth RFCOMM*/
    private BluetoothSocket mSocket;
    /*identifiant du declencher auquel est associee la tache executant le code de ce Runnable*/
    private int mMonitoredTriggerID;
    /*valeur a envoyer sur le tunnel de communication Bluetooth RFCOMM*/
    private byte mValueToSend;
    /*reference vers le flux en sortie du tunnel de communication Bluetooth RFCOMM*/
    private OutputStream mOutputStream;

    /**
     * constructeur
     *
     * @param context            le contexte d'execution. doit etre exclusivement une instance du
     *                           ControllerFragment
     * @param monitoredTriggerID identifiant du declencheur a surveiller
     * @param valueToSend        valeur a envoyer
     */
    public CommandWriter(ControllerFragment context, int monitoredTriggerID, byte valueToSend) {
        mContext = context;
        mMonitoredTriggerID = monitoredTriggerID;
        mValueToSend = valueToSend;
        mSocket = mContext.getmSocket();
        try {
            synchronized (mContext) {
                mOutputStream = mContext.getmSocket().getOutputStream();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * methode executee par la tache dont ce Runnable est attache
     */
    @Override
    public void run() {
        /*tant que le declencheur d'arret n'est pans active*/
        while (!mContext.ismStopTrigger()) {
            /*tant que le declencheur surveille est actif*/
            while (mContext.ismTrigger(mMonitoredTriggerID)) {
                /*essayer*/
                try {
                    /*verouiller sur mSocket et bloquer toutes les tâches qui la verouillent*/
                    synchronized (mSocket) {
                        /*envoyer le code a la Raspberry Pi*/
                        mOutputStream.write(new byte[]{mValueToSend});
                        if (DEBUG)
                            Log.d(TAG, "Thread No." + mMonitoredTriggerID + " sent a byte");
                    }
                    /*dormir SLEEP_TIME millisecondes*/
                    Thread.sleep(SLEEP_TIME);
                    /*gerer les erreurs generees dans le bloc d'essai*/
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
