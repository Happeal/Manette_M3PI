package com.example.i_p34lounet.manettem3pi;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {


    Button b_Haut, b_Bas, b_Droit, b_Gauche;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        b_Haut = (Button) findViewById(R.id.bt_haut);

        b_Bas = (Button) findViewById(R.id.bt_bas);

        b_Droit = (Button) findViewById(R.id.bt_droit);

        b_Gauche = (Button) findViewById(R.id.bt_gauche);

        b_Haut.setOnClickListener(bhautclick);
        b_Bas.setOnClickListener(bbasclick);
        b_Droit.setOnClickListener(bdroitclick);
        b_Gauche.setOnClickListener(bgaucheclick);



    }


    public View.OnClickListener bhautclick = new View.OnClickListener()
    {

        @Override
        public void onClick(View v) {

        }


    };

    public View.OnClickListener bbasclick = new View.OnClickListener()
    {

        @Override
        public void onClick(View v) {

        }


    };

    public View.OnClickListener bdroitclick = new View.OnClickListener()
    {
        @Override
        public void onClick(View v) {

        }
    };

    public View.OnClickListener bgaucheclick = new View.OnClickListener()
    {

        @Override
        public void onClick(View v) {

        }
    };
}
